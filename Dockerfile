FROM alpine:3.9.3

RUN mkdir -p /run/stunnel

RUN set -ex \
   && echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
   && apk add --no-cache stunnel@testing
