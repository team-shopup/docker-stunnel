# docker-stunnel
Opening a secure tunnel via stunnel managed by a docker container

Certificates and keys are in the `certs` directory
Stunnel configuration is in the `TestTunnel.cfg` file

Getting started
- `docker-compose up -d` to start the service
- `docker-compose stop` to stop the service

the exposed port is `7743`

